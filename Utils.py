from sys import stderr, stdout
from datetime import datetime
from multiprocessing import Pool

# http://stackoverflow.com/questions/32955846/in-python-is-there-an-async-equivalent-to-multiprocessing-or-concurrent-futures
#
# asyncio: not working (because my Post object is unhashable (it’s a list))
# gevent:  not in the stdlib
#
# The chosen method, multiprocessing, may become a problem if you plan to federate thousands of pilings !

def async_map(f, iterable):
    
    with Pool(len(iterable)) as p:
        return p.map(f, iterable)

def log(message, output=stdout):
    
    print(datetime.now().strftime('%Y%m%d %H:%M:%S.%f')+' '+message, file=output)
    
