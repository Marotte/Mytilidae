import sys
import requests
from lxml import etree
import re
from datetime import datetime

from Utils   import log
from Post import Post

from pprint import pprint

class Piling(dict):

    message_tag_open = re.compile('^<message>')
    message_tag_close = re.compile('</message>$', re.MULTILINE)

    def __init__(self, name = '', url = '', tmp_dir = None, timeout = 5):
    
        self['name']         = name
        self['url']          = url
        self['last']         = []
        self['server']       = ''
        self['tmp_dir']      = tmp_dir
        self['content-type'] = ''
        self['timeout']      = float(timeout)

    def writeCache(self, content):
        
        try:
            
            open(self['tmp_dir']+'/'+self['name']+'-cache','w').write(self['content-type']+'\n'+content)
            
            return True

        except Exception as e:
            
            log('! This is bad, fix me! : '+str(e))
            
            return False

    def readCache(self):
        
        try:
            
            cache_content = open(self['tmp_dir']+'/'+self['name']+'-cache').read()
            
            return cache_content
            
        except FileNotFoundError:
            
            return ''    

    def last(self):

        #~ log(self['name']+' getting posts… ('+self['url']+')')

        try:
            
            self['date'] = open(self['tmp_dir']+'/'+self['name']+'-last-modified').read()
            
        except FileNotFoundError:
            
            self['date'] = ''

        headers = {"User-Agent": '',
                   "Accept": 'application/xml, text/plain, text/tab-separated-values',
                   "Accept-Charset": 'utf-8',
                   "If-Modified-Since": self['date']
                  }
                  
        session = requests.Session()
        hit_cache = False
        
        try:
            
            response = session.get(self['url'], headers=headers, timeout=self['timeout'])
            #~ print("\nREQUEST:\n"+str(headers))
            #~ print("\nRESPONSE:\n"+str(response.headers))
            if response.status_code == 304:
                
                log(self['name']+' not modified since '+str(self['date']))
                    
                hit_cache = True
                    
                try:
                    
                    log('HTTP 304 → read from cache file')
                    cache_content = self.readCache()
                    split = cache_content.split('\n',1)
                    self['content-type'] = split[0]
                    content = split[1]
         
                except FileNotFoundError:
                    
                    log('no cache file → GET and write content to file')
                    self['content-type'] = response.headers['content-type']
                    self.writeCache(response.text)
                    content = response.text
                    
            if not hit_cache or not content:

                log('backend modified or empty cache → GET and write content to cache file')
                self['content-type'] = response.headers['content-type']
                self.writeCache(response.text)
                content = response.text

                try:
                
                    open(self['tmp_dir']+'/'+self['name']+'-last-modified','w').write(response.headers['last-modified'])
                   
                except KeyError:
                    
                    pass


            
            log(self['name']+' responded in '+str(response.elapsed))    
            
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as e:
            
            log(self['name']+' '+str(e)+' trying to read from cache')

            try:
                
                content = open(self['tmp_dir']+'/'+self['name']+'-cache').read()
         
            except FileNotFoundError:
                    
                content = self['content-type']+'\n'

        try:
        
            if self['content-type'] in ['text/xml; charset=utf-8','application/xml']:
                
                try:  
                    
                    tribune = etree.fromstring(content.encode('utf-8'))
                        
                except etree.ParseError as error:
                        
                    return(False)
                        
                for post in tribune:
                        
                    login = post[2].text
                    info = post[0].text

                    xml_message = etree.tounicode(post[1])
                    message = self.message_tag_open.sub('', xml_message)
                    message = self.message_tag_close.sub('', message)
                    message = message.replace('\n','')
                    message = message.replace('\t','')
                    message = message.replace('\u00A0','')

                    if not login:
                        
                        login = ''

                    if not info:
                        
                        info = ''
                        
                    try:

                        self['last'].append(str(Post(post.attrib['id'], post.attrib['time'], info, login, message, self['name'])))
                        
                    except TypeError:
                        
                        log('Spurious post! '+' '+str(post.attrib['id'])+' '+str(post.attrib['time'])+' '+str(info)+' '+str(login)+' '+str(message)+' '+self['name'])
                        pass    

        

                return(self['last'])

            elif self['content-type'] in ['text/plain; charset=utf-8','text/tab-separated-values;charset=UTF-8']:
                
                try:
                    
                    tribune = content.split('\n')
                    
                    for post in tribune:
                        
                        if not post:
                            
                            continue
                        
                        l_post = post.split('\t')
                       
                        self['last'].append(str(Post(*l_post, self['name'])))

                    return(self['last'])
                    
                except Exception as e:
                    
                    log(str(e))
                    return(False)

            else:
                
                log('Wrong content-type: '+self['content-type'])
                return []

        except KeyError as e:
                   
            log(str(e))
            return(self['last'])
