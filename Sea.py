from os import remove, path, mkdir
from datetime import datetime
from sqlite3 import connect
from hashlib import md5

from Utils   import log
from BML     import bml

class Sea:

    def __init__(self, size = 100, tmp_dir = '/tmp'):
    
        self.connection    = connect(':memory:')
        self.cursor        = self.connection.cursor()
        self.cursor.execute('CREATE TABLE post (id INT, time INT, info TEXT, login TEXT, message TEXT, piling TEXT, PRIMARY KEY(id,time))')
        self.posts         = []
        self.size          = size
        self.tmp_dir       = tmp_dir
        self.data          = ''
        self.digest        = ''

        if not path.isdir(self.tmp_dir):
            
            mkdir(self.tmp_dir)

    def __del__(self):
        
        self.connection.close()
        
        
    def addPosts(self, posts = []):
        
        for p in posts:

            self.cursor.execute('INSERT OR IGNORE INTO post VALUES (?,?,?,?,?,?)',tuple(p.split('\t')))
        
        self.connection.commit()
        
    def __repr__(self):

        posts = self.cursor.execute('SELECT * FROM post ORDER BY time, id').fetchall()
        _repr = ''
        
        for p in posts[-self.size:]:
            
            _repr += '\t'.join(map(str,p))+'\n'
        
        return(_repr)

    def bml(self):
        
        posts = self.cursor.execute('SELECT * FROM post ORDER BY time, id').fetchall()
        _repr = ''
        
        for p in posts[-self.size:]:
            
            _repr += str(p[0])+'\t'+str(p[1])+'\t'+str(p[2])+'\t'+str(p[3])+'\t'+bml(str(p[4]))+'\t'+str(p[5])+'\n'
        
        self.data   = _repr
        self.digest = md5(self.data.encode('utf-8')).hexdigest()

        return self.data

    def md5(self):
        
        return self.digest
    
