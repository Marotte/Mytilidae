import re
from html import escape, unescape

def bml(escaped_bml):
    
    tags = ['i','b','s','code']
    re_amp = re.compile('[[:space:]](&)|(&)[[:space:]]')
    re_ent = re.compile('(&#[0-9]{3,4};)')

    r = escaped_bml.replace('&amp;','&')
    r = r.replace('&lt;a href=','<a href=')
    r = r.replace('&lt;/a&gt;','</a>')
    r = r.replace('&gt;[url]','>[url]')
    r = r.replace('&lt;,','<,')
    r = r.replace('&lt; ','< ')

    for t in tags:
    
        r = r.replace('&lt;'+t+'&gt;','<'+t+'>')
        r = r.replace('&lt;/'+t+'&gt;','</'+t+'>')
    
    r = re_amp.sub('&amp;', r)
    r = r.replace('&quot;','"')

    for ent in re_ent.findall(r):

        val = int(ent[2:].rstrip(';'))
        if val not in [38,60,61,62,63,64]:

            r = r.replace(ent, unescape(ent)) 
    
    return r
        
        
def bmlify(wild_shit):
    
    r = escape(re.sub('\s+',' ',wild_shit.replace('\u00A0',' ').replace('\t',' ')))

    ok_tags = ['b','i','s','code','tt']
    url = re.compile('(http|ftp)://.*')
    tokens = []

    for token in r.split():

        #~ print('Token:'+token+' → ', end='')
        nt = token
        caught = False
        
        if url.match(token):
                
                nt = '<a href="'+token.replace('&amp;','&')+'">[url]</a>'
                #~ print('URL:'+nt, end='\n')
                tokens.append(nt)
                caught = True
                continue
                
        for t in ok_tags:
            
            if re.match('&lt;'+t+'&gt;.*&lt;/'+t+'&gt;', token):
                
                nt = token.replace('&lt;'+t+'&gt;','<'+t+'>')
                nt = nt.replace('&lt;/'+t+'&gt;','</'+t+'>')
                tokens.append(nt)
                caught = True
                #~ print('Full tag:'+nt, end='\n')
                break
                
            elif re.match('&lt;'+t+'&gt;.*', token):
                
                nt = token.replace('&lt;'+t+'&gt;','<'+t+'>')
                nt = nt.replace('&lt;/'+t+'&gt;','</'+t+'>')
                tokens.append(nt)
                caught = True
                #~ print('Opening tag:'+nt, end='\n')
                break
                
            elif re.match('.*&lt;/'+t+'&gt;', token):
                
                nt = token.replace('&lt;'+t+'&gt;','<'+t+'>')
                nt = nt.replace('&lt;/'+t+'&gt;','</'+t+'>')
                tokens.append(nt)
                caught = True
                #~ print('Closing tag:'+nt, end='\n')
                break

        if not caught:
            
            tokens.append(nt)
            #~ print('Kept as is:'+nt, end='\n')
    
    return(' '.join(tokens))


